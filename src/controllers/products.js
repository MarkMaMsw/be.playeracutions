const mongoose = require("mongoose");
const Product = require("../models/product");
const ObjectId = mongoose.Types.ObjectId;

//get all product
exports.index = async (ctx) => {
  ctx.status = 200;
  ctx.body = await Product.find({});
};

//show 1 product
exports.show = async (ctx) => {
  ctx.status = 200;
  ctx.body = await Product.find({ _id: ObjectId(ctx.params.id) });
  //ctx.request.id
};

//create product
exports.store = (ctx) => {
  console.log("abcd");
  let { body } = ctx.request;
  const product = new Product(body);
  product.save();
  ctx.status = 200;
  ctx.body = { message: product };
};

//update product
exports.update = async (ctx) => {
  console.log("here!");
  console.log(ctx.request.body);
  const { id } = ctx.params;
  const product = await Product.findByIdAndUpdate(
    ObjectId(id),
    ctx.request.body,
    {
      new: true,
    }
  );
  ctx.status = 200;
  ctx.body = product;
};

//delete product
exports.destroy = async (ctx) => {
  const v = await Product.findByIdAndRemove(ctx.params.id);
  ctx.status = 200;
  ctx.body = {};
};
