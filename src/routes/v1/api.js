const { router } = require("../../loaders");
const  courseControllers = require("../../controllers/courses");
const  productsControllers = require("../../controllers/products");

router.get('/courses', courseControllers.index);
router.post('/courses', courseControllers.store);
router.get('/courses/:id', courseControllers.show);
router.patch('/courses/:id', courseControllers.update);
router.delete('/courses/:id', courseControllers.destroy);


router.get('/products', productsControllers.index);
router.post('/products', productsControllers.store);
router.get('/products/:id', productsControllers.show);
router.patch('/products/:id', productsControllers.update);
router.delete('/products/:id', productsControllers.destroy);

module.exports.router = router;
