const mongosse = require('mongoose');

const productSchema = new mongosse.Schema(
    {
        seller_id: {
            type: Number,
            required: true,
        },
        buyer_id: {
            type: Number,
            required: true,
            trim: true,
        },
        game_id: {
            type: String,
            required: true,
            trim: true,
        },
        price:{
            type: Number,
            required: true,
        },
        desc: {
            type: String,
            required: true,
            trim: true,
        },

    }, {timestamps: true},
);


const Product = mongosse.model('Product', productSchema);
module.exports = Product;
